
# include <mpi.h>
# include <stdlib.h>
# include <string.h>
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <time.h>

#define _CLOCK_ID CLOCK_MONOTONIC_RAW

void loop_body ( int *S, int n ) __attribute__((always_inline));
void loop_body ( int *S, int n ) {

  int i;
  for(i = 1; i < n-1; i++) {
    S[i] = S[i] + (S[i-1] * S[i+1] / 3.f);
  }

}

void execute_benchmark
( 
  int iter,
  int size, 
  double *local_elapsed
)
{

  int i;

  int res[2];
  struct timespec tv0, tv1;

  int *S = calloc ( size, sizeof(int) );

  for (i = 0; i < iter; i++) {
    res[0] = clock_gettime( _CLOCK_ID, &tv0 ); 

    loop_body( S, size );

    res[1] = clock_gettime( _CLOCK_ID, &tv1 ); 
    if ( (res[0] < 0 || res[1] < 0) ) 
    perror("Error with gettimeofday"); 

    local_elapsed[i] = 
      ((double)(tv1.tv_sec - tv0.tv_sec )*1000000.0) +
      ((double)(tv1.tv_nsec- tv0.tv_nsec)/1000.0)    ;
  }

  free(S);

}

void calculate_efficiency 
( 
  int iter,
  double *local_elapsed
) {

  int i;

  int mpi_rank;
  MPI_Comm_rank( MPI_COMM_WORLD, &mpi_rank );

  double *min = calloc ( iter, sizeof(double) );
  double *max = calloc ( iter, sizeof(double) );

  MPI_Reduce ( 
      local_elapsed, min, iter, 
      MPI_DOUBLE, MPI_MIN, 
      0, MPI_COMM_WORLD 
  );
  MPI_Reduce (
      local_elapsed, max, iter, 
      MPI_DOUBLE, MPI_MAX, 
      0, MPI_COMM_WORLD
  );

  if ( ! mpi_rank ) {
    long double sum_min = 0;
    long double sum_max = 0;
    for (i = 0; i < iter; i++) {
      sum_min += (long double) min[i];
      sum_max += (long double) max[i];
    }

    long double eff = sum_min/sum_max;
    fprintf(stdout, "%.3Lf\n", eff);
  }

  free(min);
  free(max);

  MPI_Barrier ( MPI_COMM_WORLD );
}

int findOptimalSize(double seek) {

  int i;
  int max_iter = 1000; // Maximum number of steps to approach seek.
  int steps_count = 0; // Number of steps taken.

  int size = 10240;                    // Initial size.
  int *S = calloc(size, sizeof(int));  // Testing array.

  int niter = 10000;   // Number of iterations for each test.

  int res[2];                // Timing result for error checking.
  struct timespec tv0, tv1;  // timespec structs for timing.

  double epsilon = 0.3; // Maximum desviation form seek
  double ttime;         // Total time
  double time;          // Time per iteration

  do {


    res[0] = clock_gettime( _CLOCK_ID, &tv0 ); // Start timing
    for (i = 0; i < niter; i++) {
      loop_body(S, size);
    }
    res[1] = clock_gettime( _CLOCK_ID, &tv1 ); // End timming
    if ( (res[0] < 0 || res[1] < 0) ) perror("Error with gettimeofday"); 

    ttime =
      ((double)(tv1.tv_sec-tv0.tv_sec)*1000000.0) +
      (double)((tv1.tv_nsec-tv0.tv_nsec)/1000.0)  ;

    time = ttime / niter; // Current size time

    double dsize = 1 - ((time-seek)/(time+seek)); // Size scale factor.
    size *= dsize;                                // Scale time.

    steps_count++;

  } while ( (time > seek+epsilon || time < seek-epsilon) && steps_count < max_iter );

  free(S);

  return size;
}

struct config_t {
  int iter;
  int size;
};

void configure
(
 struct config_t *config,
 int  argc,
 char *argv[]
)
{

  // Chech number of arguments.
  if (argc != 1) {
    fprintf
      (
       stderr,
       "Usage: noise-benchmark"
      );
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  // Parse arguments.
  config->iter = 30000000;
  config->size = findOptimalSize(10.0);

}

int main (int argc, char *argv[]) {

  MPI_Init( &argc, &argv );

  int mpi_rank;
  MPI_Comm_rank( MPI_COMM_WORLD, &mpi_rank );

  struct config_t cfg;
  if (!mpi_rank) {
    configure(&cfg, argc, argv);
  }
  MPI_Bcast( &cfg, 2, MPI_INT, 0, MPI_COMM_WORLD );

  double *local_elapsed;
  local_elapsed = calloc ( cfg.iter, sizeof(double) );

  execute_benchmark(
      cfg.iter,      // Total number of iterations.
      cfg.size,      // Size of comput in kernel.
      local_elapsed  // Time/iter/cpu. Allocated @ each core.
  );

  calculate_efficiency( cfg.iter, local_elapsed );

  MPI_Finalize();
}

