MPICC		?= mpicc
CFLAGS      	?=-O0 

all: noise-benchmark

noise-benchmark:
	@mkdir -p run
	$(MPICC) -o run/noise-benchmark $(DFLAGS) $(CFLAGS) noise-benchmark.c

