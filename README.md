# Noise Benchmark

code and information on how to run the BSC system noise detector benchmark

## Compile

To compile check the Makefile first line, and set the MPICC to the MPI compiler.
Then run `make`.

## Run

After running `make` the `run` directory has been created, and it contains the 
binary. Tu run use mpirun, e.g.:
```
mpirun -n 4800 ./noise-benchmark
```

This example will only print the efficiency value.
